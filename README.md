# Welcome  to your Shell Kitchen, Bashef.

below you'll see list of cuisine dishes that might be interesting for tonights guest:


### [Omelet](./omelet/README.md)

### [Shell Kebab](./shell_kebab/README.md)

### [Spaggetti Bash](./spaggetti_bash/README.md)

### [New York Shell Steak](./new_york_shell_steak/README.md)

### [Cookie Jar](./cookie_jar/README.md)


